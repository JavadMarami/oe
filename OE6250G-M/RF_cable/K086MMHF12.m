
close all;
m= [ 141 233 322 410 496 589];
n= [ 233 322 410 496 589 689];
d = (n-m)/5
A = [m m+d m+2*d m+3*d m+4*d m+5*d];

Xi = [1 87 280 598 258 404 117]; % pixcels
Yi = [-1 -13 -21  -25  -10  -21  -5]; % pixcels
for z=2:length(Xi)
    Xi(z) = Xi(z) + Xi(z-1);
    Yi(z) = Yi(z) + Yi(z-1);
end

Xi = Xi*4.4960/175; % to real values
Yi = Yi*0.4/18; % to real values

X = [];
Y = [];
for z=1:length(Xi)-1
    X = [X linspace(Xi(z),Xi(z+1),(Xi(z+1)-Xi(z))*50)];
    Y = [Y linspace(Yi(z),Yi(z+1),(Xi(z+1)-Xi(z))*50)];
end

Xref = 0.1:0.1:36;
newXi = zeros(1,length(Xref));
newYi = zeros(1,length(Xref));
for z=1:length(Xref)
    temp = abs(X-Xref(z));
    [value index] = sort(temp);
    newXi(z) = Xref(z);
    newYi(z) = Y(index(1));
end

figure;plot(X,Y,'.-');
xlabel('Freq [Hz]');
ylabel('dB');
grid on;
ylim([-12 4]);

filename = ['K086MMHF-12.csv'];
for z=1:length(newXi)
    dlmwrite(filename,{[num2str(newXi(z)) ' , ' num2str(newYi(z)) ' , 0 ' ]},'delimiter','','-append');
end
