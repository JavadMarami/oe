function x = integrate(y);
% x = integrate(y);
% this function is used to make step out of an impulse response
% y is a vector
% x is the result of integration
% Author: Anirudh Sureka
% Copyright: Teledyne Lecroy 
x = y;
for i = 2:length(x)
    x(i) = x(i-1) + x(i);
end