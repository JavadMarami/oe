function [b_final,a_final,bd,ad,b_b,a_b] = besselWorth(Nbutter,Fs,fcb,fc,besselOrder,plotFig,FunctionType,besselAttenAtFc,butterAttenAtFc,finalAttenAtfcb);
% making a besselWorth low pass fitler
%[b_final,a_final] = besselWorth(Nbutter,Fs,fcb,fc,besselOrder,plotFig,FunctionType);
% OR for function type = 1
%[b_final,a_final] = besselWorth(Nbutter,Fs,fcb,fc,besselOrder,plotFig,FunctionType,besselAttenAtFc,butterAttenAtFc,finalAttenAtfcb)
% Nbutter: takes the order of the butterworth filter or 0
% Fs is the sample rate of the filter
% fcb is the cut-off freq. of butterworth filter OR stop band freq for
%       butter.
% fc  is the cut-off freq. of analog bessel filter OR pass band cutoff fc
% besselOrder = order of the analog bessel filter or 0
% BesselAttenAtFc = bessel filter attenuation at fc eg -2
% butterAttenAtFc = butter filter atten at fc eg -40
% finalAttenAtfcb = Atten at fcb which is stop band freq eg -40
% FunctionType = 0 to use old way; 1 for more constrained design
% b_final = combined numerator
% a_final = combined denominator
% bd = digital bessel
% b_b = digital butter

%OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
%
% -----------------------------------------------------------------------------
%
%	besselWorth.m
%
% -----------------------------------------------------------------------------
%
%    Written by: Anirudh Sureka, LeCroy Inc., Chestnut Ridge, NY
%
%    NOTE:  Returns a bessel filter with controlled atten at bandwidth and
%    butter worth filter with controlled atten at bandwidth and stop band
%
%    Started:	12 Sept. 2008
%
%OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
%*/

%% make the bessel filter
cutoff = 2*pi*fc;

if besselOrder ~= 0
    if(FunctionType == 0)
        [b,a] = besself(besselOrder,cutoff);
        [bd,ad] = bilinear(b,a,Fs);
    else
        % here we make sure by iterating that at freq fc we have specified
        % attenuation
        [bd,ad] = besselDigital(besselOrder,fc,Fs,besselAttenAtFc,plotFig);
    end
else
    bd = 1; ad = 1;
end


%%  Make the butter filter
if (FunctionType == 0)
    if Nbutter ~= 0
        [b_b,a_b] = butter(Nbutter, fcb/(Fs/2));
    else
        b_b = 1;
        a_b = 1;
    end
else
    h = freqz(bd,ad,[0;fcb],Fs);
    besselAttenAtfcb = Powerlog(h(2));
    if (besselAttenAtfcb > finalAttenAtfcb)
        b_b = 1;
        a_b = 1;
    else
        butterAttenAtfcb = finalAttenAtfcb - besselAttenAtfcb;
        [b_b,a_b] = butterConstrained(fc,-butterAttenAtFc,fcb,-butterAttenAtfcb,Fs,0);
    end
end

%% combine both filters
b_final = conv(bd,b_b);
a_final = conv(ad,a_b);

%% debug plots
if plotFig

    [h,f] = freqz(b_final,a_final,512,Fs);
    [hbe,fbe] = freqz(bd,ad,512,Fs);
    [hb,fb] = freqz(b_b,a_b,512,Fs);    
    
    figure(2123);clf;    
    subplot(211)
    plot(f,Powerlog(h)); hold on;
    plot(fbe,Powerlog(hbe),'r-');
    plot(fb,Powerlog(hb),'k-');
    if(FunctionType == 1)
        line([0 Fs/2],[-besselAttenAtFc -besselAttenAtFc],'Color','r','LineStyle','--');
    end
    axis([0 Fs/2 -50 0]); grid on; title('digital final besselWorth');
    legend('final','bessel','butter');
    
%     subplot(212)
%     gd = grpdelay_fromH(h,0,f,plotFig);
%     
%     plot(f,gd); grid on; title('gd');
end