function [f,mag,phasInDeg,H] = ReadSpectrumTxt(filename,plotFig);
% function [f,mag,phasInDeg,H] = ReadSpectrumTxt(filename,plotFig);
% input: filename - spectrum.txt file which have no text headers and have 
%                   [f mag phaseindeg]
%        plotFig - 1 to plot debug plot
% output: f = freq read
%         mag = mag read
%         phasInDeg = phase in degrees read
%         H = complex vector made from mag and phase in degrees
% Author: Anirudh Sureka
% Copyright: Teledyne Lecroy 

data = dlmread(filename,',');
data = data(:,1:3);
sizeRead = data(1,1);
f = data(2:sizeRead+1,1);
spectrum = data(2:sizeRead+1,2);

spectrum_new_abs = abs(spectrum);
spectrum_new_angle = unwrap(angle(spectrum));

spectrum_new_abs = smooth(spectrum_new_abs,10);
spectrum_new_angle = smooth(spectrum_new_angle,10);

spectrum_new = spectrum_new_abs.*exp(1i*(spectrum_new_angle));

% figure;plot(f,20*log10(abs(spectrum)),'*');hold on;plot(f,20*log10(abs(spectrum_new)));
% figure;plot(f,unwrap(angle(spectrum)),'*');hold on;plot(f,unwrap(angle(spectrum_new)));

mag = abs(spectrum_new);
phas = angle(spectrum_new);

sizeRead = length(spectrum_new);

if(sizeRead ~= length(f))
    error('Anirudh: the ReadSpectrum.txt function does not have the same number of elements as said to have.');
end

H = mag.*exp(1i*phas);

phasInDeg = zeros(1,sizeRead);%phas.*180/pi;

if(plotFig)
    figure(3); 
    subplot(211);
    hold on;
    plot(f,Powerlog(mag),'r-'); grid on; title('Mag response of spectrum.txt');
    
    subplot(212);
    hold on;
    plot(f,unwrap(phas),'r:'); grid on; title('phase response');
    
%    subplot(313);
%    hold on;
%    fir = ifft(H);
%    plot(f,grpdelay(H,1,f,160),'r-'); grid on; title('group delay');
end
  
