function x = stepdelay(y,Fs);
% x = stepdelay(y,Fs);
% this function takes in a positive going step and calculates 
% the delay in time(sec) at which the rising edge occurs
% y = vector having positive going step; if a 2 col vector the 2nd col. is
% used
% Fs = sample Rate
% Author: Anirudh Sureka
% Copyright: Teledyne Lecroy 

timeIdx = [];

y = makeColMatrix(y);
if cols(y) == 2
    y = y(:,2);
end

[Counts, Centers] = hist(y,128);

firstHalfCounts = Counts(1:64);
firstHalfCenters = Centers(1:64);
[maxCount, maxCountIndx] = max(firstHalfCounts);
bottom = firstHalfCenters(maxCountIndx);

secondHalfCounts = Counts(65:end);
secondHalfCenters = Centers(65:end);
[maxCount, maxCountIndx] = max(secondHalfCounts);
top = secondHalfCenters(maxCountIndx);

thres = (top + bottom) / 2;

for i = 2:length(y)
    if( (y(i-1)<=thres) & (y(i)>=thres) )
        timeIdx = (thres-y(i-1))/(y(i) - y(i-1)) + i - 1;
    end

    if(~isempty(timeIdx))
        break;
    end
end

x = timeIdx/Fs;
