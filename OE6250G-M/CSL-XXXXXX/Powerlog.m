function  x = Powerlog(x,Normalize);
% x = 20log10(x,Normalize);
% this function is used for plotting the power spectrum in log scale
% this function takes in vector x, complex or real, takes its abs. and then
% take its log base 10 and multiplies it by 20
% Normalize-> if the second argument is given and is 1 then the data is
%             normalized against max value before being plot
%          -> if the second argument is givne and is 2 then the data is
%             normalized against first value

%OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
%
% -----------------------------------------------------------------------------
%
%	powerlog.m
%
% -----------------------------------------------------------------------------
%
%    Written by: Anirudh Sureka, LeCroy Inc., Chestnut Ridge, NY
%
%    NOTE:  returns 20 log10 of the input value or vector
%
%    Started:	12 Sept. 2008
%
%OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO*/

Norm_abx = abs(x);

% if asked to normalize the data 
if(nargin==2)
    abx = abs(x);
    if (Normalize == 1)
        Norm_abx = abx/max(abx);
    else
        Norm_abx = abx/abx(1);
    end
end
        
x = 20*log10(Norm_abx);








