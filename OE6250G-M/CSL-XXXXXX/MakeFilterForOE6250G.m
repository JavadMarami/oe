
allHandle = allchild(0);
allTag = get(allHandle, 'Tag');
isMsgbox = strncmp(allTag, 'Msgbox_', 7);
delete(allHandle(isMsgbox));
  
close all;
clear ;
clc;

filename = ['FrequencyResponse.csv'];
fileID = fopen(filename);
if fileID < 0
    disp('cannot find FrequencyResponse.csv file in this folder');
    error;
end
fclose(fileID);
        
% verify the serial number
serialNumber = ['OE Serial Number :,' '[a-z0-9A-Z]*'];
extractedStr = regexp(fileread(filename), serialNumber, 'match');
extractedStr = extractedStr{1};
extractedStr =textscan(extractedStr, '%*s %*s %*s :, %s,');
serialNumber = extractedStr{1};serialNumber = serialNumber{1};
serialNumber = str2num(serialNumber);
if (isempty(serialNumber))
	disp('FrequencyResponse.csv is not correct, please copy the vendor provided file');
	error;
else
    disp(['the vendor provided data for serial number ' num2str(serialNumber) ' is verified']);
end

% reading DC Gains
dcGain = ['DC gain @ 850nm 4.6V :,' '[a-z0-9A-Z.-+]*'];
extractedStr = regexp(fileread(filename), dcGain, 'match');
extractedStr = extractedStr{1};
extractedStr =textscan(extractedStr, 'DC gain @ 850nm 4.6V :,%n,');
dcGain_850 = num2str(extractedStr{1});

dcGain = ['DC gain @ 1310nm 4.6V :,' '[a-z0-9A-Z.-+]*'];
extractedStr = regexp(fileread(filename), dcGain, 'match');
extractedStr = extractedStr{1};
extractedStr =textscan(extractedStr, 'DC gain @ 1310nm 4.6V :,%n,');
dcGain_1310 = num2str(extractedStr{1});

dcGain = ['DC gain @ 1550nm 4.6V :,' '[a-z0-9A-Z.-+]*'];
extractedStr = regexp(fileread(filename), dcGain, 'match');
extractedStr = extractedStr{1};
extractedStr =textscan(extractedStr, 'DC gain @ 1550nm 4.6V :,%n,');
dcGain_1550 = num2str(extractedStr{1});

% read the frequency response
readData = csvread(filename,10,0);
f = readData(:,1);
phase = readData(:,7);
phase = phase - phase(1);
amp_dB = readData(:,6);
amp = 10.^(amp_dB/20);
spectrum = amp.*exp(1i*phase);

fig=figure(2000);
plot(f,20*log10(abs(spectrum)));
xlim([0 37]);
ylim([-8 2]);
xlabel('Freq [GHz]');
ylabel('Power [dB]');
grid on;
title(['Raw Frequency response for optical module SN = ' num2str(serialNumber)]);
saveas(fig,'RawFrequencyResponse.jpg')

RFfilename = 'RFCable.csv';
fileID = fopen(RFfilename);
if fileID < 0
    disp('cannot find RFCable.csv file in this folder and the compensation filter would not include it');

else
    fclose(fileID);
    disp('======== the RF cable frequency response will be included to the filters (if you do not need this, remove the RFCable.csv from this folder =========)');
    % read the RF cable frequency response
    readData = csvread(RFfilename,0,0);
    rf_amp_dB = readData(:,2);
    fig = figure(7000);
    plot(f,rf_amp_dB,'-');
    xlabel('Freq [GHz]');
    ylabel('dB');
    grid on;
    ylim([-12 4]);
    title('RF cable frequency response');
    saveas(fig,'RF_CableFrequencyResponse.jpg')
    
    new_amp_db = amp_dB+rf_amp_dB;
    new_amp = 10.^(new_amp_db/20);
    spectrum = new_amp.*exp(1i*phase);
    
    fig=figure(7001);
    plot(f,20*log10(abs(spectrum)));
    xlim([0 37]);
    ylim([-8 2]);
    xlabel('Freq [GHz]');
    ylabel('Power [dB]');
    grid on;
    title('RF cable + Raw frequency response');
    saveas(fig,'RF_and_RawFrequencyResponse.jpg')
end

fig = figure(2001);
plot(f,unwrap(angle(spectrum)));
xlabel('Freq [GHz]');
ylabel('Phase [rd]');
grid on;
title(['Raw Phase response for optical module SN = ' num2str(serialNumber)]);
saveas(fig,'RawPhaseResponse.jpg')
close all;

% smooth the spectrum
spectrum_new_abs = abs(spectrum);
spectrum_new_angle = unwrap(angle(spectrum));

spectrum_new_abs = smooth(spectrum_new_abs,10);
spectrum_new_angle = smooth(spectrum_new_angle,10);

spectrum_new = spectrum_new_abs.*exp(1i*(spectrum_new_angle));
spectrum_new = [f,spectrum_new];

% read the config file and update the variables
config_file = strcat('OE6250G_MatlabConfigFile.txt');
        
[HiLimits,LoLimits,LimitFreq,MaxBW,...
    ShaperButterOrder,ShaperBesselOrder,ShaperBesselAttenAtCutoff,ShaperButterAttenAtCutoff,...
    ShaperAttenAtStopBandStart,ShaperCutoff,ShaperStopBandStart,...
    TotalTaps_10, TrimLeft_10, TrimRight_10, TotalTaps_20, TrimLeft_20, TrimRight_20, ...
    TotalTaps_40, TrimLeft_40, TrimRight_40, TotalTaps_60, TrimLeft_60, TrimRight_60,...
    TotalTaps_80, TrimLeft_80, TrimRight_80, TotalTaps_100, TrimLeft_100, TrimRight_100, TotalTaps_160, TrimLeft_160, TrimRight_160,FitDontCareStartFreq...
    BesselRollOffOrder, BesselRollOffFreq, FilterGainLimit,FilterAttenLimit,OutputHiLimits,OutputLoLimits,...
    PhaseInterval,SpectrumLimitFreq ]...
    = ReadLimitsFromConfigFileTalon(config_file);
LimitFreq(3) = MaxBW;
LimitFreq(4) = 250;
numVdiv = 0;

% check the raw response to be in limits
figureHandle = 1500;
SubplotNo = 311;
[PassRaw, fig]= CheckFreqResponse(LimitFreq,HiLimits,LoLimits,spectrum_new,1,figureHandle,SubplotNo);
if PassRaw == 0
    title('RawResponse LimitCheck : FAIL!');
    disp('Raw Reponse does not fit inside the limits! Filter build might fail. report it to Javad!');
    saveas(fig,'RawReponseDoesNotFit.jpg')
    pause;
    error
else
    title(strcat('RawResponse LimitCheck : PASS!'));
end
        
% make filters-----------------------------------------
FsV = [10,20,40,60,80,100,160];
PassOutputAll = 1;
PassFilterAll = 1;
for iFs = 1:length(FsV)
    Fs = FsV(iFs);
    disp(['filter processing is started for ', num2str(Fs), ' GS/s']);
    if Fs/2 < MaxBW
        BW = Fs/2;
    else
        BW = MaxBW;
    end
    
    LimitFreq(3) = BW;
    LimitFreq(4) = Fs/2;
    
    % limit spectrum to the points needed and calculate the step from
    % spectrum
    spectrum = limitSpectrum(spectrum_new,spectrum_new(:,1),Fs/2,0,1);
    f = spectrum(:,1);
    spectrum = spectrum(:,2);
    spectrum = spectrum./spectrum(1);
                
    if MaxBW < Fs/2
        % make the final system response - ideal bessel- butterworth
        % response
        [b_final,a_final] = besselWorth(ShaperButterOrder,Fs,ShaperStopBandStart,ShaperCutoff,ShaperBesselOrder,0,...
            1,ShaperBesselAttenAtCutoff,ShaperButterAttenAtCutoff,ShaperAttenAtStopBandStart);%16G filter
        Sshaper = Spec(b_final,a_final,f,Fs,0,0);
        Sshaper = Sshaper(:,2);
    else
        Sshaper = ones(length(f),1);
    end
    D = (1./(spectrum)).*abs(Sshaper);
    fir = idft(D,f,Fs);
    fir = fir/sum(fir);
    fir = fftshift(fir);
    
    Stest = Spec(fir,1,f,Fs,0,0);
    h= Stest(:,2);
    gd = Stest(:,3);
                
    SubplotNo = 312;
    PassOutput = CheckFreqResponse(LimitFreq,OutputHiLimits,OutputLoLimits,[f h.*spectrum],1,figureHandle,SubplotNo);
    PassOutputAll = (PassOutputAll & PassOutput);
    title('Compensated Output');
    axis([0 Fs/2 -5 +5]);
                
    SubplotNo = 313;
    PassFilter = CheckFreqResponse([MaxBW;Fs/2],[FilterGainLimit;FilterGainLimit],[-FilterAttenLimit;-1000],[f h],1,figureHandle,SubplotNo);
    PassFilterAll = (PassFilter & PassFilterAll);
    title('Filters Reponse');
    
    if (PassOutput == 0)
        disp([num2str(Fs) ' GS/sec Filtered Output Freq Reponse does not fit inside the limits!']);
        saveas(fig,[num2str(Fs),'GS_FilterDidNotCreatedProperly.jpg'])
        disp(['for ' num2str(Fs) 'GS, Filter has not created inside the limits! Filter build failed. report it to Javad!']);
        pause;
        error
    end
    if (PassFilter == 0)
        disp([num2str(Fs) ' GS/sec Filtered Output Freq Reponse does not fit inside the limits!']);
        saveas(fig,[num2str(Fs),'GS_FilterDidNotCreatedProperly.jpg'])
        disp(['for ' num2str(Fs) 'GS, Filter has not created inside the limits! Filter build failed. report it to Javad!']);
        pause;
        error
    end
    saveas(fig,[num2str(Fs),'GS_filterResults_1.jpg']);
    
    % write the filter coeff to a file
    filename = strcat('Cached_fir_',num2str(numVdiv),'_',num2str(Fs),'.txt');
    WriteFIRtoDS(fir,Fs,strcat(filename));
    
    if MaxBW < Fs/2
        [bb,ab] = besselDigital(4,SpectrumLimitFreq,Fs,-3,0);
        hb = freqz(bb,ab,f,Fs);
    else
        hb = ones(length(h),1);
    end
    fig2 = figure(iFs+100); hold on;
    subplot(2,2,1)
    plot(f,Powerlog(D),'b-',f,Powerlog(h),'r-',f,Powerlog(Sshaper),'m-',f,Powerlog(hb.*h),'g-'); grid on;
    axis([0 Fs/2 -50 20]);
    legend('Specification','filter','noiseFilt','withBessel','Location','best'); % title('PowerSpectrum')
    
    subplot(2,2,2)
    plot(f,Powerlog(h)+Powerlog(spectrum)); grid on;
    axis([0 Fs/2 -8 1]);
    legend('finalOutput')
    
    subplot(2,2,3)
    plot(fir); grid on; title('filter taps');
    
    subplot(2,2,4)
    plot(f,Powerlog(D),'b-',f,Powerlog(h),'r-',f,Powerlog(Sshaper),'m-'); grid on;
    axis([0 FitDontCareStartFreq -5 10]);
    legend('Specification','filter','noiseFilt','Location','best'); title('PowerSpectrum')
    
    gcf = fig2;
    subtitle(['The output result for OE serial number = (' num2str(Fs) 'GS/s)']);
    saveas(fig2,[num2str(Fs),'GS_filterResults_2.jpg']);
    disp(['filter processing is finished for ', num2str(Fs), ' GS/s']);
%     pause(3);
    close(fig2);
    
    if Fs == 160
        [~, serialNumber] = fileparts(pwd);
        [status,cmdout] = system(['del /f ' serialNumber '_Calibration_Frequency_Response.jpg']);
        fig6 = figure;
        scrsz = get(0,'ScreenSize');
        set(fig6,'position',scrsz);
        plot(f,20*log10(abs(h.*spectrum)),f,20*log10(abs(hb.*h.*spectrum))); axis([0 40 -8 +1]);grid on;
        legend({'Compensated Frequency Response up to 35 GHz','User-defined Bessel Filter enabled in UI at 25 GHz'},'FontSize',14); 
        xlabel('Freq [GHz]');
        ylabel('Power [dB]');
        title('Frequency Response');
        saveas(fig6,[serialNumber '_Calibration_Frequency_Response'],'fig');
        close(fig6);
    end
end

if (PassOutputAll & PassFilterAll)
    PASS = 1;
    disp('--------------------ALL FILTERS PASS!---------------------------------');
else
    disp('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx');
    disp('--------------------FAIL! -- Talk to Javad ---------------');
    disp('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx');
end

% create the hex file
[status,cmdout] = system(['del output.hex /f']);
[status,cmdout] = system('..\CreateHex.exe 10,Cached_fir_0_10.txt;20,Cached_fir_0_20.txt;40,Cached_fir_0_40.txt;60,Cached_fir_0_60.txt;80,Cached_fir_0_80.txt;100,Cached_fir_0_100.txt;160,Cached_fir_0_160.txt','-echo');
disp('hex file is created');

% rename the hex file and xml file
[~, serialNumber] = fileparts(pwd);
[status,cmdout] = system(['del /f OE6250GM_' serialNumber '.hex']);
[status,cmdout] = system(['ren output.hex OE6250GM_' serialNumber '.hex'],'-echo');
[status,cmdout] = system(['copy /y OE6250GM.xml OE6250GM_' serialNumber '.xml'],'-echo');

% update the xml file
xmlData = xmlread(fullfile(['OE6250GM_' serialNumber '.xml']));
allListItems=xmlData.getElementsByTagName('Amplifier');
allListItems=allListItems.item(0);
allListItems.setAttribute('Name_SN',['OE6250GM_' serialNumber]);
allListItems=xmlData.getElementsByTagName('AmplifierInfo');
allListItems=allListItems.item(0);
allListItems.setAttribute('Gains',[dcGain_850 ',' dcGain_1310 ',' dcGain_1550]);
allListItems.setAttribute('HexFile',['OE6250GM_' serialNumber '.hex']);
xmlwrite(fullfile(['OE6250GM_' serialNumber '.xml']),xmlData);
disp('xml file is updated');

% zip 
disp('zipping files are started');
zip(['OE6250GM_' serialNumber '.zip'],{['OE6250GM_' serialNumber '.xml'],['OE6250GM_' serialNumber '.hex']});
disp('zip file is created');

disp('--------------PASS! the zip file is ready to program on OE ---------------');

serial = char(dec2hex(uint8(serialNumber),2));
newSerial = [];
for z=1:length(serial)
    newSerial = [newSerial serial(z,:)];
end
PIF(1,:) = ['010C004F4536323530472D4D04' num2str(dec2hex(length(newSerial)/2+2,2)) newSerial '11066F12033B1803'];
PIF(2,:) = '5725060000000036030038085858586E6D0A40030062060000C841FFFFFFFFFF';
PIF(3,:) = 'FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF';
PIF(4,:) = 'FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF';
PIF(5,:) = 'FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF';
PIF(6,:) = 'FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF';
PIF(7,:) = 'FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF';
PIF(8,:) = 'FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF';


outFilename = ['PIF_OE6250GM_' serialNumber '.csv'];
dlmwrite(outFilename,{''},'delimiter','');
for z=1:8
    dlmwrite(outFilename,{char(PIF(z,:))},'delimiter','','-append');
end
disp('--------------the PIF file is ready to program on OE ---------------');


outFilename = ['Program_Zip_File_for_OE6250GM_' serialNumber '.bat'];
dlmwrite(outFilename,{'pause'},'delimiter','');
dlmwrite(outFilename,{['ProbeUtil.js "WriteZipFileToProbe(''OE6250GM_' serialNumber '.zip'',''c2'',''1,2,3,4,5,6,7'',1,''24C512'');"']},'delimiter','','-append');
dlmwrite(outFilename,{'pause'},'delimiter','','-append');

disp('--------------the bat file is ready to program the OE ---------------');
disp('--------------USE the manual to finalize the programming-------------');
disp('-------------- DONE !-------------');

pause

