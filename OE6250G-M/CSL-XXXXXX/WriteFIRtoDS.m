function WriteFIRtoDS(firCoeffs,fs,filename);
%   WriteFIRtoDS(firCoeffs,fs,filename);
% this function writes the fir coeffs into a text file for the scope to
% read and apply
%   firCoeffs: is the FIR filter
%   fs: sampling freq. of the filter
%   filename: the name in which the filter file would be stored
% Author: Anirudh Sureka
% Copyright: Teledyne Lecroy 

N = length(firCoeffs);

fid = fopen(filename,'wt');
fprintf(fid,'%e 1 %e %d ',fs,fs,N);   % writing the sample rate and stages


for i = 1:N
    fprintf(fid,'%e ',firCoeffs(i));   % writing the num. coeff
end

fprintf(fid,'1 1.00');  %writing the denominator
fclose(fid);
