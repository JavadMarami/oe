function     [b_b,a_b] = butterConstrained(fpass,attenPass,fstop,attenStop,Fs,plotFig);
% function [b_b,a_b] = butterConstrained(fc,butterAttenAtFc,fcb,butterAttenAtfcb,Fs,plotFig);
%   This function generates a butter worth filter with the constraints at 2
%   freq of meeting specified attenuations; any order; 
% fpassband = freq1
% fstopband = freq2
% attenPass = max atten in passband; +ve atten in dB
% attenStop = min atten in stopband; +ve atten in dB
% Fs = digital sample rate

%OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
%
% -----------------------------------------------------------------------------
%
%	butterConstrained.m
%
% -----------------------------------------------------------------------------
%
%    Written by: Anirudh Sureka, LeCroy Inc., Chestnut Ridge, NY
%
%    NOTE:  returns a specific digital butterworth filter with atten
%    specified at bandwidth and stop band
%
%    Started:	12 Sept. 2008
%
%OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
%*/

Nyq = Fs/2;
if fstop > Nyq
    fstopNew = Nyq*0.99;
    attenStop = fstopNew * attenStop/fstop;
    fstop = fstopNew;
end

[n,wn] = buttord(fpass/Nyq,fstop/Nyq,attenPass,attenStop);
[b_b,a_b] = butter(n,wn);

if plotFig
    figure(234324); clf;
    freqz(b_b,a_b,256,Fs);
    title('Func butterConstrained:');
    subplot(211)
    axis([0 1.1*fstop -(attenStop*1.1) 1]);
end

