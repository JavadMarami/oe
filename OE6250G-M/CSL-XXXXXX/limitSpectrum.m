function [h,f] = limitSpectrum(h,f,limitFreq,epsilon,PadLastValue);
% [h,f] = limitSpectrum(h,f,limitFreq,epsilon);
% this function limits the half positive spectrum
% h = is the larger spectrum; it can be multi col. vector
% f =  freq. vector
% limitFreq = all freq. would be less than or equal to this freq.
% epsilon = used in limitFreq comparison
% PadLastValue = if 1 then we would repeat the last ori freq value till the
%               limitFreq value
% Author: Anirudh Sureka
% Copyright: Teledyne Lecroy 

error(nargchk(3, 5, nargin, 'struct'));

if nargin == 3
    epsilon = 0;
    PadLastValue = 0;
elseif nargin == 4
    PadLastValue = 0;
end

indx = find(f <= (limitFreq+epsilon));
if length(indx) == 0
    return;
else
    h = h(indx,:);
    f = f(indx,:);
end

if PadLastValue == 1
    if (f(end) < limitFreq)
        deltaF = f(2) - f(1);
        if(mod(limitFreq,deltaF) > 0)
            error('Function limitSpectrum : limitFreq is not a multiple of deltaF!');
            return;
        end
        Npadded = limitFreq/deltaF;
        fnew = (f(1):deltaF:limitFreq+deltaF)';
        HPad = ones(Npadded+1 -length(f),1)*h(end);
        hnew = [h(:,2);HPad];
        
        if false
            figure(123111); clf;
            plot(f,powerlog(h(:,2))); grid on; hold on;
            plot(fnew,powerlog(hnew),'r-');
            legend('ori','padded');
        end
        
        f = fnew;
        h = [fnew, hnew];
    end
end
        