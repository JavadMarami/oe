function x = makeColVector(X);
%  x = makeColVector(X);
%   this function takes in a vector and makes is a column vector
%   X = it may be a column or a row vector
%   x =  is a Col Vector

%OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
%
% -----------------------------------------------------------------------------
%
%	makeColVector.m
%
% -----------------------------------------------------------------------------
%
%    Written by: Anirudh Sureka, LeCroy Inc., Chestnut Ridge, NY
%
%    NOTE:  makes a row vector a column vector
%
%    Started:	12 Sept. 2008
%
%OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
%*/

[r,c] = size(X);

if(r ~= 1 &  c ~= 1)
    error('Anirudh: input is a matrix, should be a vector!');
end


x = X;

if(c > r)
    x = X.';
end