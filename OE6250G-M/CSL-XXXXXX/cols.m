function x = cols(mat);
% returns the no. of columns in a matrix

%OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
%
% -----------------------------------------------------------------------------
%
%	cols.m
%
% -----------------------------------------------------------------------------
%
%    Written by: Anirudh Sureka, LeCroy Inc., Chestnut Ridge, NY
%
%    NOTE:  returns no. of columns in a matrix
%
%    Started:	12 Sept. 2008
%
%OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO*/

y = size(mat);
x = y(2);