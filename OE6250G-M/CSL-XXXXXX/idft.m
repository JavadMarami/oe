function x = idft(X, freq, Fs);
% x = idft(X, freq, Fs);  OR x = idft(X,NyquistPresent)
% this function takes the +ve spectrum and calculates the inverse fourier
% transform by considering +ve and -ve spectrums complex conjugates
%       X = +ve spectrum column vector with or without the Nyquist Frequency = Fs/2
%       freq =  is the +ve freq vector
%       Fs =  is the sampling Frequency
%       x = REAL data values got from the inverse transform

%OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
%
% -----------------------------------------------------------------------------
%
%	idft.m
%
% -----------------------------------------------------------------------------
%
%    Written by: Anirudh Sureka, LeCroy Inc., Chestnut Ridge, NY
%
%    NOTE:  gives the inverse fft just by taking the +ve spectrum 
%
%    Started:	12 Sept. 2008
%
%OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
%*/

error(nargchk(2,3,nargin));
X = makeColVector(X);

if nargin == 2 
    NyquistFreqPresent = freq;
else
    indx = find(freq == Fs/2);
    if(length(indx) == 1)
        NyquistFreqPresent = 1;
    else
        NyquistFreqPresent = 0;
    end
    
    indx = find(freq <= Fs/2);
    X = X(indx);
end



% Note: the first point of X is DC 
if(NyquistFreqPresent)
%     if(abs(imag(X(end))) > 1e-8)          % the nyquist freq. pt should be real
%         error('Anirudh: the nyquist freq. pt has big imaginary component!');
%     end
    Xfull = [X; conj(flipud(X(2:end-1)))];
else
    Xfull = [X; conj(flipud(X(2:end)))];
end

x = ifft(Xfull);


% check if the imaginary values are very small as x should be all real data

maxImagValue = max(abs(imag(x)));
if(maxImagValue > 1e-2)
    error('Anirudh: result which should be real vector has high imaginary values!')
end

x = real(x);




