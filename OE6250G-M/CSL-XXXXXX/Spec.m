function H = Spec(num,deno,f,fs,IsSOS,PlotFig);
% H = Spec(num,deno,f,fs,IsSOS,PlotFig);
% this function calculated the complex freq. response and the groupdelay of
% the filter
% num :  numerator or can be a 6 cols of multiple stages for SOS
% deno: denominator OR junk
% f :  vector of frequencies OR a number 
% fs: sampling freq
% IsSOS : if its a second order system
% plotFig:
% Result H: a 3 column Vector with 1st col as freq. 2nd col as complex
% freq. response and 3rd col as gd in seconds
% Author: Anirudh Sureka
% Copyright: Teledyne Lecroy 

% calculate the freq vector 
if(length(f) == 1)
    f = freq(2*f,fs,1);
end

f = makeColVector(f);

if(IsSOS)
    sos = num;
    [h,gd] = freqzSOS(sos,f,fs);
else
    h = freqz(num,deno,f,fs);
    gd = grpdelay(num,deno,f,fs);
end

gd = gd/fs;

if(PlotFig)
    figure(141124); clf;
    subplot(211)
    plot(f,Powerlog(h)); grid on; title('Spec: Mag Resp')
    subplot(212)
    plot(f,gd); grid on;  title('GD in ns');
end

H = [f,h,gd];