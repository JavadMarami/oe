function x_val = bisector(xV,yV, yDesired, plotFig);
%   function x_val = bisector(xV,yV, yDesired, plotFig);
% This function bisects the value of nearest x values to get desired y values.
% If desired value is outside the range of y values then limit range values are returned; no
% extrapolation done.
% xV = x-axis values vector; should be atleast 2 elements
% yV = y-axis values vector; should be atleast 2 elements
% yDesired = the value for which we need to find the x value
% x_val = output value computed from linear interpolation of values


% xV and yV must have at least two values; the minX and maxX values with
% respective Y values

%OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
%
% -----------------------------------------------------------------------------
%
%	bisector.m
%
% -----------------------------------------------------------------------------
%
%    Written by: Anirudh Sureka, LeCroy Inc., Chestnut Ridge, NY
%
%    NOTE:  implements bisection of values
%
%    Started:	12 Sept. 2008
%
%OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
%*/


xV = makeColVector(xV);
yV = makeColVector(yV);
if(rows(xV) < 2)
    error('function bisector: x and y vectors should have atleast two values!');
end

if rows(xV) ~= rows(yV)
    error('function bisection: x and y vectors should be of equal size');
end

[yssV,Iy] = sort(yV);

xssV = xV(Iy);

if yDesired <= yssV(1)       % yDesired is less than min value
    x_val = xssV(1);
    disp('Function bisection: desired value is lesser than min value, x value capped');
elseif yDesired >= yssV(end) % yDesired is greater than max value
    x_val = xssV(end);
    disp('Function bisection: desired value is more than max value, x value capped');
else
    % desired value is between the max and min values
    [yAppended,indx] = sort([yssV;yDesired]);
    I = find(yAppended == yDesired);
    
%     % using the line equation to find point
%     x_val = (yDesired-yssV(I-1))*(xssV(I) - xssV(I-1))/(yssV(I) - yssV(I-1)) + xssV(I-1);
    
    % using the midpoint of two pts
    x_val = (xssV(I) + xssV(I-1))/2;
end

if plotFig
    figure(32151); clf;
    plot(xV,yV); grid on; hold on;
    plot(x_val,yDesired,'ro');
    title('function: bisector');
end
