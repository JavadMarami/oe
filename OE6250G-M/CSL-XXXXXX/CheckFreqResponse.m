function [Pass, fig]= CheckFreqResponse(limitFreqVector,HiLimitValues,LoLimitValues,H,debugPlots,figureHandle,SubplotNo);
% function Pass = CheckFreqResponse(limitFreqVector,HiLimitValues,LoLimitValues,H,debugPlots,figureHandle,SubplotNo);
%   this function takes in a freq reponse and checks such that its within
%   limits
% Pass = true; when response is within limits
% limitFreqVector = list of freq. where values are stated
% HiLimitValues = Hi limit values; 
% LoLimitValues = Lo limit values; 
% H = 2 col. vector with [freq; magnitude]
% debugPlots = 1 to plot debug plots
% figureHandle = figure no on which debug plot is made
% subplotNo = subplot where the figure is made
% Author: Anirudh Sureka
% Copyright: Teledyne Lecroy 
error(nargchk(4, 7, nargin, 'struct'));

f = H(:,1);
HinDB = Powerlog(H(:,2));
Pass = 1;
j = 1;

failf = [];
failVal = [];

for i = 1:length(f)
    if(f(i) <= limitFreqVector(j))
        if((HinDB(i) >= HiLimitValues(j)) || (HinDB(i) <= LoLimitValues(j)))
            Pass = 0;
            failf = [failf;f(i)];
            failVal = [failVal;HinDB(i)];
        end
    else
        j = j+1;
        i = i-1;
    end
end

if debugPlots
    fig = figure(figureHandle); subplot(SubplotNo);
    stairs([0;limitFreqVector],[HiLimitValues;HiLimitValues(end)]); hold on;
    stairs([0;limitFreqVector],[LoLimitValues;HiLimitValues(end)]); grid on;
    plot(f,HinDB,'r-');
    plot(failf,failVal,'ro');
    axis([0 f(end)+0.01*f(end) -50 10]);
    if Pass
        titleString = ('Func CheckFreqResponse:PASS');
    else
        titleString = ('Func CheckFreqResponse:FAIL!!');
    end
    title(titleString);
    hold off;
end

