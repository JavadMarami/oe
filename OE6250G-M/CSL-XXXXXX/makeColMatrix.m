function x = makeColMatrix(X);
%  x = makeColMatrix(X);
%   this function takes in a matrix and makes is a column oriented matrix
%   X = it may be a column or a row oriented matrix; 
%   x =  is a Col oriented matrix
% Author: Anirudh Sureka
% Copyright: Teledyne Lecroy 

[r,c] = size(X);

x = X;

if(c > r)
    x = X.';
end