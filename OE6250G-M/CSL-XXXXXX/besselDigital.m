function [bz,az] = besselDigital(besselOrder,fc,Fs,Attenc,plotFig);
% function [b,a] = besselDigital(besselOrder,fc,Fs,Attenc,plotFig);
%   This function generates by bisection a digital bessel filter which has
%   given attenuation at a given frequency
% besselOrder = order of the bessel filter
% fc = digital freq at which Attenc is needed
% Fs = digital sample rate
% Attenc = attenuation at freq fc

%OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
%
% -----------------------------------------------------------------------------
%
%	besselDigital.m
%
% -----------------------------------------------------------------------------
%
%    Written by: Anirudh Sureka, LeCroy Inc., Chestnut Ridge, NY
%
%    NOTE:  returns a digital bessel filter with required attenuation and
%    order at a specified frequency
%
%    Started:	12 Sept. 2008
%
%OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
%*/

tol = 0.05;

% analogBW to start off with
xanalogBW = [0.1:1:5]*pi*fc;
ydBValAtfc = xanalogBW;

for iter = 1:length(xanalogBW)
    [b,a] = besself(besselOrder,xanalogBW(iter)); 
    z = roots(b);
    p = roots(a);

    % impulse invar method
    [bz,az] = impinvar(b,a,Fs,0.00001);
    bz = bz./sum(bz);
    az = az./sum(az);

    h = freqz(bz,az,[0 fc],Fs);
    ydBValAtfc(iter) = Powerlog(h(2));
    
    if plotFig
        freqz(bz,az,256,Fs);
        subplot(211);hold on;
        plot(fc,Attenc,'r-x');
        title('Function besselDigital');
    end
end

for iter = 1:10
    newAnalogBW = bisector(xanalogBW,ydBValAtfc, Attenc, plotFig);

    [b,a] = besself(besselOrder,newAnalogBW); 
    z = roots(b);
    p = roots(a);

    % impulse invar method
    [bz,az] = impinvar(b,a,Fs,0.001);
    bz = bz./sum(bz);
    az = az./sum(az);

    h = freqz(bz,az,[0 fc],Fs);
    val = Powerlog(h(2));
    
    if plotFig 
        freqz(bz,az,256,Fs);
        subplot(211);hold on;
        plot(fc,Attenc,'r-x');    
        title('Function besselDigital');
    end
    if abs(val - Attenc) < tol
        break
    else
        xanalogBW = [xanalogBW,newAnalogBW];
        ydBValAtfc = [ydBValAtfc,val];
    end
end
done = 1;