function [HiLimits,LoLimits,LimitFreq,Bandwidth,...
    ShaperButterOrder,ShaperBesselOrder,ShaperBesselAttenAtCutoff,ShaperButterAttenAtCutoff,...
    ShaperAttenAtStopBandStart,ShaperCutoff,ShaperStopBandStart,...
    TotalTaps_10, TrimLeft_10, TrimRight_10, TotalTaps_20, TrimLeft_20, TrimRight_20, ...
    TotalTaps_40, TrimLeft_40, TrimRight_40, TotalTaps_60, TrimLeft_60, TrimRight_60,...
    TotalTaps_80, TrimLeft_80, TrimRight_80, TotalTaps_100, TrimLeft_100, TrimRight_100 , TotalTaps_160, TrimLeft_160, TrimRight_160,FitDontCareStartFreq...
    BesselRollOffOrder, BesselRollOffFreq, FilterGainLimit,FilterAttenLimit,OutputHiLimits,OutputLoLimits,...
    PhaseInterval,SpectrumLimitFreq ]...
= ReadLimitsFromConfigFileTalon(filename);
% This function reads the config file and returns the variables needed for
% the filter build 

%OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
%
% -----------------------------------------------------------------------------
%
%	ReadLimitsFromConfigFile.m
%
% -----------------------------------------------------------------------------
%
%    Written by: Anirudh Sureka, LeCroy Inc., Chestnut Ridge, NY
%
%    NOTE:  Reads variables from config file for ProbeFilter build
%
%    Started:	30 June 2009
%
%OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
%*/

fid = fopen(filename);
if fid == -1
    errordlg(strcat('Cannot find file: ',filename));
end
data = textscan(fid,'%s%s%f');  
fclose(fid);

values = data{3};
stringArray = data{1};

% remove the lines with comments in them.  Comments start with %
commentIndexes = [];
for i = 1:length(stringArray)
    if strmatch('%',stringArray{i})
        commentIndexes = [commentIndexes;i];
    end
end
for i = 1:length(commentIndexes)
    stringArray{commentIndexes(i)} = '';
    values(commentIndexes(i)) = 123456;
end

HiLimits = zeros(4,1);
LoLimits = zeros(4,1);
LimitFreq = zeros(4,1);
OutputHiLimits = zeros(4,1);
OutputLoLimits = zeros(4,1);

HiLimits(1) = values(findIndexOfString('RawResponsedBHiLimit1',stringArray));
HiLimits(2) = values(findIndexOfString('RawResponsedBHiLimit2',stringArray));
HiLimits(3) = values(findIndexOfString('RawResponsedBHiLimit3',stringArray));
HiLimits(4) = values(findIndexOfString('RawResponsedBHiLimit4',stringArray));

LoLimits(1) = values(findIndexOfString('RawResponsedBLoLimit1',stringArray));
LoLimits(2) = values(findIndexOfString('RawResponsedBLoLimit2',stringArray));
LoLimits(3) = values(findIndexOfString('RawResponsedBLoLimit3',stringArray));
LoLimits(4) = values(findIndexOfString('RawResponsedBLoLimit4',stringArray));

LimitFreq(1) = values(findIndexOfString('RawResponseFreqLimit1',stringArray));
LimitFreq(2) = values(findIndexOfString('RawResponseFreqLimit2',stringArray));
LimitFreq(3) = 6.1;
LimitFreq(4) = 10;

OutputHiLimits(1) = values(findIndexOfString('OutputResponsedBHiLimit1',stringArray));
OutputHiLimits(2) = values(findIndexOfString('OutputResponsedBHiLimit2',stringArray));
OutputHiLimits(3) = values(findIndexOfString('OutputResponsedBHiLimit3',stringArray));
OutputHiLimits(4) = values(findIndexOfString('OutputResponsedBHiLimit4',stringArray));

OutputLoLimits(1) = values(findIndexOfString('OutputResponsedBLoLimit1',stringArray));
OutputLoLimits(2) = values(findIndexOfString('OutputResponsedBLoLimit2',stringArray));
OutputLoLimits(3) = values(findIndexOfString('OutputResponsedBLoLimit3',stringArray));
OutputLoLimits(4) = values(findIndexOfString('OutputResponsedBLoLimit4',stringArray));

SpectrumLimitFreq = values(findIndexOfString('SpectrumLimitFreq',stringArray));
Bandwidth = values(findIndexOfString('Bandwidth',stringArray));
ShaperButterOrder = values(findIndexOfString('ShaperButterOrder',stringArray));
ShaperBesselOrder = values(findIndexOfString('ShaperBesselOrder',stringArray));
ShaperBesselAttenAtCutoff = values(findIndexOfString('ShaperBesselAttenAtCutoff',stringArray));
ShaperButterAttenAtCutoff = values(findIndexOfString('ShaperButterAttenAtCutoff',stringArray));
ShaperAttenAtStopBandStart = values(findIndexOfString('ShaperAttenAtStopBandStart',stringArray));
ShaperCutoff = values(findIndexOfString('ShaperCutoff',stringArray));
ShaperStopBandStart = values(findIndexOfString('ShaperStopBandStart',stringArray));

TotalTaps_10 = values(findIndexOfString('TotalTaps_10',stringArray));
TrimLeft_10 = values(findIndexOfString('TrimLeft_10',stringArray));
TrimRight_10 = values(findIndexOfString('TrimRight_10',stringArray));
TotalTaps_20 = values(findIndexOfString('TotalTaps_20',stringArray));
TrimLeft_20 = values(findIndexOfString('TrimLeft_20',stringArray));
TrimRight_20 = values(findIndexOfString('TrimRight_20',stringArray));
TotalTaps_40 = values(findIndexOfString('TotalTaps_40',stringArray));
TrimLeft_40 = values(findIndexOfString('TrimLeft_40',stringArray));
TrimRight_40 = values(findIndexOfString('TrimRight_40',stringArray));
TotalTaps_60 = values(findIndexOfString('TotalTaps_60',stringArray));
TrimLeft_60 = values(findIndexOfString('TrimLeft_60',stringArray));
TrimRight_60 = values(findIndexOfString('TrimRight_60',stringArray));
TotalTaps_80 = values(findIndexOfString('TotalTaps_80',stringArray));
TrimLeft_80 = values(findIndexOfString('TrimLeft_80',stringArray));
TrimRight_80 = values(findIndexOfString('TrimRight_80',stringArray));
TotalTaps_100 = values(findIndexOfString('TotalTaps_100',stringArray));
TrimLeft_100 = values(findIndexOfString('TrimLeft_100',stringArray));
TrimRight_100 = values(findIndexOfString('TrimRight_100',stringArray));
TotalTaps_160 = values(findIndexOfString('TotalTaps_160',stringArray));
TrimLeft_160 = values(findIndexOfString('TrimLeft_160',stringArray));
TrimRight_160 = values(findIndexOfString('TrimRight_160',stringArray));

FitDontCareStartFreq = values(findIndexOfString('FitDontCareStartFreq',stringArray));
BesselRollOffOrder = values(findIndexOfString('BesselRollOffOrder',stringArray));
BesselRollOffFreq = values(findIndexOfString('BesselRollOffFreq',stringArray));
FilterGainLimit = values(findIndexOfString('FilterGainLimit',stringArray));
FilterAttenLimit = values(findIndexOfString('FilterAttenLimit',stringArray));
PhaseInterval = values(findIndexOfString('PhaseInterval',stringArray));
if 0
    figure(123213);clf;
    stairs([0;LimitFreq],[HiLimits;HiLimits(end)]); grid on; hold on;
    stairs([0;LimitFreq],[LoLimits;LoLimits(end)]);
end

function indx = findIndexOfString(str,cellArray);

for i = 1:length(cellArray)
    if strmatch(str,cellArray{i})
        indx = i;
        break;
    end
end