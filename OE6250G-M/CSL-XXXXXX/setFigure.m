function setFigure();
% this function sets the position and size of the figure windows to create
% bigger windows
% Author: Anirudh Sureka
% Copyright: Teledyne Lecroy 

position = [0 100 1400 800];set(0, 'DefaultFigurePosition', position);
legend('Location','best');