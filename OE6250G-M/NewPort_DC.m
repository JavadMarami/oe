close all;
clear;

% define the serial number
debug = 0;

for serial_number=1:1000
    try
        load(['P000',num2str(serial_number),'/DC_Measurements.mat']);
    catch
        break;
    end
    OutputVoltageArray_V(serial_number,:) = measuredVoltage_mV;
end

inputPower_Dbm = -28.5:0.5:10;

for sn=1:serial_number-1
    inputPower_mW = 10.^(inputPower_Dbm/10);
    outputVoltage_mV = OutputVoltageArray_V(sn,:)*1000;
    
    outputVoltage_mV_linear = outputVoltage_mV(abs(outputVoltage_mV) <= 500);
    inputPower_mW_linear = inputPower_mW(1:length(outputVoltage_mV_linear));

    % to find the slope and offset
    beta(sn,:) = polyfit(inputPower_mW_linear,outputVoltage_mV_linear,1);  % to find the linear fit only between -5dBm to +5dBm
    titleStr(sn,:) = ['DC measurements (all points less than 0.5V (up to ' num2str(inputPower_mW_linear(end)) 'mW) are used for linear fit in SN00' num2str(sn)];
    
    % the results in V/mW
    figure(1);
    p(sn) = plot(inputPower_mW,outputVoltage_mV,'--*');
    hold on;
    plot(inputPower_mW,(beta(sn,1)*inputPower_mW+beta(sn,2)),':r');
    xlabel('Input Power [mW]');
    ylabel('Output Voltage [mV]');
    legendStr(sn,:) = ['SN ' num2str(sn) ' Conversion Gain = ' num2str(beta(sn,1)) ' V/W ; Dark Power = ' num2str(beta(sn,2)) ' mV'];
    if debug == 1
        legend(legendStr(sn,:));
        title(titleStr(sn,:));
    end
    ylim([-1100 100]);
    grid on
end
legend(p,legendStr);
title(['DC Conersion Gain']);
clear p;
clear legendStr;

for sn=1:serial_number-1
    inputPower_mW = 10.^(inputPower_Dbm/10);
    outputVoltage_mV = OutputVoltageArray_V(sn,:)*1000;

    linearOutput_mV = polyval(beta(sn,:),inputPower_mW);
    error_mV = 100*(linearOutput_mV - outputVoltage_mV)./linearOutput_mV;
    figure(2);
    hold on;
    p(sn,:) = plot(inputPower_mW,error_mV,'--*');
    xlim([0.1 10])
    ylim([-10 +3])
    xlabel('Input Power [mW]');
    ylabel('Deviation  [%]');
    legendStr(sn,:) = ['SN ' num2str(sn) ' Non Linear DC Conversion Gain error % '];
    grid on
end
legend(p,legendStr);
title(['DC Conersion Gain Error %']);
clear p;
clear legendStr;

for sn=1:serial_number-1
    inputPower_mW = 10.^(inputPower_Dbm/10);
    outputVoltage_mV = OutputVoltageArray_V(sn,:)*1000;

    inputPowerMeasured_mW = (outputVoltage_mV-beta(sn,2))/beta(sn,1);
    error_mW = inputPower_mW - inputPowerMeasured_mW;
    figure(3);
    hold on;
    p(sn) = plot(inputPower_mW,error_mW,'-*');
    legendStr(sn,:) = ['SN ' num2str(sn) ' Non Linear DC Conversion Gain error mW'];
    xlabel('Input Power [mW]');
    ylabel('power error [mW]');
    grid on
end
legend(p,legendStr);
title(['DC Conersion Gain Error mW']);
clear p;
clear legendStr;


