clear ;
close all;

filename = ['Freq Response_from_Vendor.csv'];
fileID = fopen(filename);
if fileID < 0
    disp('cannot find Freq Response_from_Vendor.csv file in this folder');
    error;
end
fclose(fileID);
        
% read the frequency response
readData = csvread(filename,2,0); % after I save the xlsx to csv the location of data has changed
f = readData(:,1)'; % the first col has the frequency
rawSpectrum = readData(:,2)'; % the second col has the complex value for freq resp and phase, I just use the phase resp of this col
amp_dB = readData(:,5)'; % this col has the scope compensated freq resp values

rawPhase = angle(rawSpectrum);

valid_size = 36; % we just need the freq up to 36 GHz
f = f(1:valid_size);
rawPhase = rawPhase(1:valid_size);
amp_dB = amp_dB(1:valid_size);

amp = 10.^(amp_dB/20); % to be able to work on spectrum we need to convert it from dB domain to the real domain
spectrum = amp.*exp(1i*rawPhase); % here is the spectrum

upSample = 10; % the resolution is not enough, so we upsample to 100 MHz

fTimeStep = 1;
localTime = (0:fTimeStep:fTimeStep*(valid_size-1));
fTimeStepNew = fTimeStep/upSample;
localTimeNew = (0:fTimeStepNew:fTimeStep*valid_size-fTimeStepNew);
spectrum_new_abs = interp1(localTime,abs(spectrum),localTimeNew,'linear','extrap');
spectrum_new_angle = interp1(localTime,unwrap(angle(spectrum)),localTimeNew,'linear','extrap');
new_f = interp1(localTime,f,localTimeNew,'linear','extrap');
spectrum_new = spectrum_new_abs.*exp(1j*(spectrum_new_angle));

% verify if the upsampling did not affect the original data
% figure;plot(f,20*log10(abs(spectrum)),'*');hold on;plot(new_f,20*log10(abs(spectrum_new)));
% figure;plot(f,unwrap(angle(spectrum)),'*');hold on;plot(new_f,unwrap(angle(spectrum_new)));

% to remove the delay from the spectrum in time domain I removed the slope
% of the phase
b = polyfit(new_f(1:360)',spectrum_new_angle(1:360)',1);
spectrum_new = spectrum_new.*exp(-1j*new_f*b(1));
spectrum_new = spectrum_new.*exp(-1j*b(2));

% to verify the delay removal
% figure;plot(f,20*log10(abs(spectrum)),'*');hold on;plot(new_f,20*log10(abs(spectrum_new)));
% figure;plot(f,unwrap(angle(spectrum)),'*');hold on;plot(new_f,unwrap(angle(spectrum_new)));

% the smooth process will happened in the next step
% spectrum_new_abs = smooth(spectrum_new_abs,10)';
% spectrum_new_angle = smooth(spectrum_new_angle,10)';

% make the resolution 100 Mhz
f = 0:0.1:35.9;
spectrum = spectrum_new;
filename = 'FrequencyResponse.csv';
fileID = fopen(filename,'w');
fprintf(fileID,'%s \n','OE Serial Number :,1,,,,,');
fprintf(fileID,'%s \n','Manufactured date :,xxxx,,,,,');
fprintf(fileID,'%s \n','DC gain @ 850nm 4.6V :,-83.2,,,,,');
fprintf(fileID,'%s \n','DC gain @ 1310nm 4.6V :,-132.000000,,,,,');
fprintf(fileID,'%s \n','DC gain @ 1550nm 4.6V :,-126.5,,,,,');
fprintf(fileID,'%s \n',',,,,,,');
fprintf(fileID,'%s \n',',Frequency Response of the OE module,,,,,');
fprintf(fileID,'%s \n',',850nm 4.6V,,1310nm 4.6V,,1550nm 4.6V,');
fprintf(fileID,'%s \n','GHz,Mag_dB,Phase_rad,Mag_dB,Phase_rad,Mag_dB,Phase_rad');
for z=1:length(f)
    dlmwrite(filename,{[num2str(f(z)) ',0,0,0,0,' num2str(20*log10(abs(spectrum(z)))) ' , ' num2str(angle(spectrum(z)))]},'delimiter','','-append');
end

fig=figure;
plot(f,20*log10(abs(spectrum)));
xlim([0 37]);
ylim([-8 2]);
xlabel('Freq [GHz]');
ylabel('Power [dB]');
grid on;
title(['Raw Frequency response for optical module SN = 1']);
saveas(fig,'RawFrequencyResponse.jpg')

fig = figure;
plot(f,unwrap(angle(spectrum)));
xlabel('Freq [GHz]');
ylabel('Phase [rd]');
grid on;
title(['Raw Phase response for optical module SN = 1']);
saveas(fig,'RawPhaseResponse.jpg')
close all;