function [ output_args ] = Untitled( input_args )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
clear ;
close all;

% define the serial number
debug = 0;

for serial_number=1:1000
    try
        load(['P000',num2str(serial_number),'/Frequency_Response.mat']);
    catch
        break;
    end
    freqX_array(serial_number,:) = freqX;
    measuredFreqResp_array(serial_number,:) = measuredFreqResp;
    scopeCorrected_array(serial_number,:) = scopeCorrected;
    XiMagnitude_array(serial_number,:) = XiMagnitude;
    FrequencyAxis_Hz_array(serial_number,:) = FrequencyAxis_Hz;
end

arraySize = 200;

for sn=1:serial_number-1
    freqX = freqX_array(sn,:);
    measuredFreqResp = measuredFreqResp_array(sn,:);
    scopeCorrected = scopeCorrected_array(sn,:);
    XiMagnitude = XiMagnitude_array(sn,:);
    FrequencyAxis_Hz = FrequencyAxis_Hz_array(sn,:);

    freqX = freqX(1:arraySize);
    fftMeasuredFreqResp = fftshift(measuredFreqResp);
    freqResp_Anirudh = (fftMeasuredFreqResp(length(fftMeasuredFreqResp)/2+1:length(fftMeasuredFreqResp)/2+arraySize));
    freqResp = (20*log10(abs(fftMeasuredFreqResp(length(fftMeasuredFreqResp)/2+1:length(fftMeasuredFreqResp)/2+arraySize))))-(20*log10(abs(fftMeasuredFreqResp(length(fftMeasuredFreqResp)/2+1))));
    figure;plot(freqX,freqResp,'r');
    %M = ((20*log10(abs(fftMeasuredFreqResp(length(fftMeasuredFreqResp)/2+1:length(fftMeasuredFreqResp)/2+50))))-(20*log10(abs(fftMeasuredFreqResp(length(fftMeasuredFreqResp)/2+1)))));
    hold on;
    plot(freqX(1:50),scopeCorrected(1:50),'b');
    
    spectrum(:,1) = freqX;
    spectrum(:,2) = freqResp_Anirudh;
    
    filename = ['P000',num2str(sn),'/spectrum_0.txt'];
    dlmwrite(filename,{num2str(length(freqX))},'delimiter','');
    for z=1:length(freqX')
        dlmwrite(filename,{[num2str(spectrum(z,1)) ' , ' num2str(spectrum(z,2)) ' , 0 ' ]},'delimiter','','-append');
    end
    
    XiMagnitude_db = 20*log10(XiMagnitude);
    XiMagnitude_db = smooth(XiMagnitude_db,10);
    
    plot(FrequencyAxis_Hz/1e9,XiMagnitude_db,'c');
    legend('newPort: raw data', 'newPort: after scope compensation', 'cs: measured raw data');
    xlabel('Freq [GHz]');
    ylabel('Power [dB]');
    grid on;
    title(['Raw Frequency response for optical module SN = ' num2str(sn)]);
    % xlim([0 200])
    % ylim([-5 +1])
    
    % figure;plot(freqX(1:50),(scopeCorrected(1:50)-(20*log10(abs(fftMeasuredFreqResp(length(fftMeasuredFreqResp)/2+1:length(fftMeasuredFreqResp)/2+50))))+(20*log10(abs(fftMeasuredFreqResp(length(fftMeasuredFreqResp)/2+1))))));
    % Diff = (scopeCorrected(1:50)-(20*log10(abs(fftMeasuredFreqResp(length(fftMeasuredFreqResp)/2+1:length(fftMeasuredFreqResp)/2+50))))+(20*log10(abs(fftMeasuredFreqResp(length(fftMeasuredFreqResp)/2+1)))));
    % xlabel('GHz');
    % ylabel('dBe');
    %
    % E = M + Diff - scopeCorrected(1:50);
    % figure;plot(freqX(1:50),E);
end

